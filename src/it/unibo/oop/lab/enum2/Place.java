package it.unibo.oop.lab.enum2;

public enum Place {
	indoor,
	outdoor,
	both;
}
