/**
 * 
 */
package it.unibo.oop.lab.enum2;
import static it.unibo.oop.lab.enum2.Place.*;
/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */

public enum Sport {

	basket(indoor, 10, "basket"),
	volley(indoor, 10, "volley"),
	tennis(both, 2, "tennis"),
	bike(outdoor, 1, "bike"),
	F1(outdoor, 1, "F1"),
	motogp(outdoor, 1, "motogp"),
	soccer(both, 12, "soccer");
	
	private int noTeamMembers;
	private String actualName;
	private Place place;
	
	Sport(final Place place, final int noTeamMembers, final String actualName){
		this.place = place;
		this.noTeamMembers = noTeamMembers;
		this.actualName = actualName;
	}
	
	public boolean isIndividualSport() {
		return this.noTeamMembers == 1;
	}
	
	public boolean isIndoorSport() {
		return this.place == Place.indoor;
	}
	
	public Place getPlace() {
		return this.place;
	}

	public String toString() {
		return this.actualName;
	}

}
